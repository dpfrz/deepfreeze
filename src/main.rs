#![feature(proc_macro_hygiene, decl_macro)]
#![feature(plugin)]

const OWNER: &'static str = include_str!("../static/.dpfrz-owner.conf");
const OWNID: &'static str = include_str!("../static/.dpfrz-owner.conf");

extern crate rocket;
extern crate rocket_contrib;

use rocket::*;
use rocket_contrib::templates::Template;

use std::collections::HashMap;

fn context<'a>() -> HashMap<&'a str, u64> {
    HashMap::from([
        // ("a", 1),
    ])
}

#[get("/hi/<nick>/<numb>")]
fn hi(nick: String, numb: String) -> Template {
    let ctx = context();

    println!("OWNER=\"{}\"", OWNER);

    println!("hi: {} : {}", nick, numb);

    if nick == OWNER.lines().next().unwrap() && numb == OWNID.lines().nth_back(0).unwrap() {
        return Template::render("hi", &ctx);
    } else {
        return Template::render("index", &ctx);
    }
}

#[get("/")]
fn index() -> Template {
    let ctx = context();
    Template::render("index", &ctx)
}

fn main() {
    let rock = rocket::ignite().attach(Template::fairing());

    let routes = routes![index, hi];

    rock.mount("/", routes).launch();
}
