#!/bin/bash

deepfreeze_commit() {
  local params="$@"

  local namespaces=( 
    "deepfreeze.cool"
    "deepfreeze.house"
    "deepfreeze.me"
    "deepfreeze.media"
    "deepfreeze.money"
    "deepfreeze.space"
    "deepfreeze.tel"
    "deepfreeze.wtf" 
  )

  for namespace in namespaces; do
    local current_dir="$PWD"
    
    if [ -d "$namespace" ]; then
      cd "$namespace"

      git add .
      git commit -m "$params"
      git push    

      cd "$current_dir"
    fi
  done

  git add .
  git commit -m "$params"
  git push
}

deepfreeze_commit "$@"
